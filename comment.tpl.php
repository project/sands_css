  <div class="comment <?php if ($new) echo "comment-new"; ?>">
    <?php if ($picture) { print $picture; } ?>
    <h3 class="title"><?php print $title; ?></h3>
    <div class="info">
    <?php if ($new) { ?><span class="new">new</span><?php } ?>
      <span class="submitted">
			  <span class="user"><?php print theme('username', $comment); ?></span> -
        <span class="date"><?php print format_date($comment->timestamp, $type="large"); ?></span>
      </span>
    </div>
    <div class="content"><?php print $content; ?></div>
    <?php if ($links) { ?><div class="links">&raquo; <?php print $links; ?></div><?php } ?>
    <div class="clear"></div>
  </div>
