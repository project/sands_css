<?php
  // Generate CSS classes for the node
  $classes = array();
  if ($sticky)
    $classes[] = 'sticky';
  if (!$page) {
    $classes[] = 'item-listing';
    $classes[] = $zebra;
    $classes[] = "node-list-$seqid";
  }
  $classes = ' ' . implode(' ', $classes);
?>
  <div class="node<?php print $classes; ?>">
    <?php if ($picture) print $picture; ?>
    <?php if (!$page) { ?><h2 class="title"><a href="<?php print $node_url?>" title="<?php print $title?>" rel="bookmark"><?php print $title?></a></h2><?php }; ?>
    <div class="info">
      <?php if ($submitted) { ?>
      <span class="submitted">
			  <span class="user"><?php print theme('username', $node); ?></span> -
        <span class="date"><?php print format_date($node->created, $type="large"); ?></span>
      </span>
      <?php } ?>
      <?php if ($terms) { ?> <span class="taxonomy">Tags: <?php print $terms?></span> <?php } ?>
    </div>
    <div class="content">
      <?php print $content?>
    </div>
    <?php if ($links) { ?><div class="links">&raquo; <?php print $links?></div><?php } ?>
    <div class="clear"></div>
  </div>
